from os import listdir 
from os.path import isfile, join
import re

serverurl = "http://tp47.gitlab.io/cmcmemes/memes/"
localurl = "http://localhost:1313/hugo/memes/"
baseurl = serverurl

document_top  = """<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>CMC Memes Gallery</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

  <!-- Link Swiper's CSS -->
  <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css">

  <!-- Demo styles -->
  <style>
    html, body {
      position: relative;
      height: 100%;
    }
    body {
      background: #000;
      font-family: Helvetica Neue, Helvetica, Arial, sans-serif;
      font-size: 14px;
      color:#000;
      margin: 0;
      padding: 0;
    }
    .swiper-container {
      width: 100%;
      height: 100%;
    }
    .swiper-slide {
      text-align: center;
      font-size: 18px;
      background: #000;

    }
    .swiper-slide img {
      width: auto;
      height: auto;
      max-width: 100%;
      max-height: 100%;
      -ms-transform: translate(-50%, -50%);
      -webkit-transform: translate(-50%, -50%);
      -moz-transform: translate(-50%, -50%);
      transform: translate(-50%, -50%);
      position: absolute;
      left: 50%;
      top: 50%;
    }
  </style>
</head>
<body>
  <!-- Swiper -->
  <div class="swiper-container">
    <div class="swiper-wrapper">
"""

document_bottom = """
    </div>
    <!-- Add Pagination -->
    <div class="swiper-pagination swiper-pagination-white"></div>
    <!-- Navigation -->
    <div class="swiper-button-next swiper-button-white"></div>
    <div class="swiper-button-prev swiper-button-white"></div>
  </div>

  <!-- Swiper JS -->
  <script src="https://unpkg.com/swiper/js/swiper.min.js"></script>

  <!-- Initialize Swiper -->
  <script>
    var swiper = new Swiper('.swiper-container', {
      // Enable lazy loading
      lazy: true,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },

    });
  </script>
</body>
</html>"""






def getmemesnippets():
    memedir = "../static/memes/"
    
    # gets all the files in memedir
    memefiles = [f for f in listdir(memedir) if isfile(join(memedir, f))]
    print(memefiles)
    # memes = list of tuples of title, path
    memes = []
    for file in memefiles:
        title = " ".join(re.split(r"[_\s-]+", file))[:-4]
        path = baseurl + file
        path = path.replace(" ", "")
        memes.append((title, path))

    snippets = []
    for meme in memes:
        snippet = f"""<div class="swiper-slide">
<img alt="{meme[0]}" data-src="{meme[1]}" class="swiper-lazy">
<div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
 </div>"""
        snippets.append(snippet)

    return snippets

index = open("../static/gallery.html", "w")
index.write(document_top)
count = 0
for snippet in getmemesnippets():
    index.write(snippet)
    count=count + 1

index.write(document_bottom)
index.close()
print("wrote "+str(count)+" memes")
